#include <cstring>
#include <iostream>
#include <string>
#include <zmq.hpp>

int main(int argc, char** argv)
{
    if (argc != 3)
    {
        std::cout << "Please provide 2 parameters.\nUsage: echo_client <ip> <port>\n";
        return 1;
    }

    std::string ip(argv[1]);
    std::string port(argv[2]);

    zmq::context_t context(1);
    zmq::socket_t socket(context, ZMQ_REQ);
    socket.connect("tcp://" + ip + ":" + port);

    std::cout << "Client starts.\n";

    while (true)
    {
        // Receive a string from cin.
        std::cout << "req: ";
        std::string requested_string;
        std::getline(std::cin, requested_string);

        // Convert to a message.
        zmq::message_t request(requested_string.size());
        std::memcpy(request.data(), requested_string.data(), requested_string.size());

        // Send the request.
        socket.send(request);

        // Receive the reply.
        zmq::message_t reply;
        socket.recv(&reply);

        // Convert to a string.
        std::string replied_string((char*) reply.data(), reply.size());
        std::cout << "rep: " + replied_string + "\n";
    }

    return 0;
}