#include <cstring>
#include <iostream>
#include <string>
#include <zmq.hpp>

int main(int argc, char** argv)
{
    if (argc != 2)
    {
        std::cout << "Please provide 1 parameter.\nUsage: echo_server <port>\n";
        return 1;
    }

    std::string port(argv[1]);

    zmq::context_t context(1);
    zmq::socket_t socket(context, ZMQ_REP);
    socket.bind("tcp://*:" + port);

    std::cout << "Server starts.\n";

    while (true)
    {
        // Receive a request.
        zmq::message_t request;
        socket.recv(&request);

        // Convert the message to a string.
        std::string requested_string((char*) request.data(), request.size());
        std::cout << "req: " + requested_string + "\n";

        // Prepare the string to be replied.
        auto replied_string = requested_string;
        std::cout << "rep: " + replied_string + "\n";

        // Convert to a message.
        zmq::message_t reply(replied_string.size());
        std::memcpy(reply.data(), replied_string.data(), replied_string.size());

        // Send the reply.
        socket.send(reply);
    }

    return 0;
}